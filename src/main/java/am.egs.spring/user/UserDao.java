package am.egs.spring.user;

import java.sql.SQLException;


public interface UserDao
{
    public boolean isValidUser(String username, String password) throws SQLException;
}